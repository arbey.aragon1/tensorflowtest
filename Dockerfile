FROM jupyter/tensorflow-notebook
MAINTAINER ARBEY arbey.aragon@gmail.com

RUN pip install --upgrade pip
COPY requirements.txt /tmp/
RUN pip install --requirement /tmp/requirements.txt && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

EXPOSE 8888

CMD bash
